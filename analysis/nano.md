
# How to use?

输入 `nano py_datacamp/test.Rmd` 出现以下界面

![](pic/nano_write.png)<!-- -->

输入文字退出，使用 <kbd>control</kbd> + <kbd>X</kbd>

![](pic/nano_confirm.png)<!-- -->

点击 <kbd>Y</kbd> 退出。

![](pic/nano_save.png)<!-- -->

确认名字，点击 <kbd>Enter</kbd>

![](pic/nano_save_proof.png)<!-- -->

文件成功保存

<input type="checkbox" id="checkbox1" class="styled">整理nano的东西出来
