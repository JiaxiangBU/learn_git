
在`output: html_document`中，

内容为

    test
    
    https://jiaxiangbu.github.io/learn_git/a20190112154639.html
    
    work

在 Github 上设置

``` r
knitr::include_graphics("pic/github_pages.png")
```

![](pic/github_pages.png)<!-- -->

展示为

``` r
knitr::include_graphics("pic/preview_html.png")
```

![](pic/preview_html.png)<!-- -->

最后输入 `https://jiaxiangbu.github.io/learn_git/a20190112154639.html`

发现可以展示。
