
打开 <https://github.com/settings/notifications> 看到 *Email notification
preferences*，选择好 *Default notification email*，那么GitHub
上的交互会通过邮件发送，直接回复邮件即可。

1.  使用的权限参考
    <https://github.blog/2011-03-10-reply-to-comments-from-email/>
2.  特性可以参考
    <https://github.blog/2011-04-09-issues-2-0-the-next-generation/#email-issues>
3.  例子可以参考
    <https://github.com/kassambara/survminer/issues/355#issuecomment-460036758>
