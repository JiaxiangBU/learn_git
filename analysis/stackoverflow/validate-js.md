解决 Stack Overflow 的红色警告
================

![Stack Overflow requires external JavaScript from another domain, which
is blocked or failed to
load.](https://user-images.githubusercontent.com/15884785/57583969-ae8a5400-7508-11e9-9626-d7fdd070a0b4.png)

参考 [www.jianshu.com](https://www.jianshu.com/p/40715dcac309)

> stackoverflow.com 的 jQuery 文件是引用了 ajax.googleapis.com，而 google
> 在本朝是众所周知的。

Stack Overflow 出现红色警告的去除方式即本地建一个。

<http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js>
是打不开的，需要用一台可以打开的电脑，本地保存到
`~/ajax/libs/jquery/1.7.1/jquery.min.js`。 这里也提供下载地址
[jquery.min.js](jquery.min.js)

然后 Python 建立一个静态页面。

``` shell
python -m http.server 80
```

上面是 Python 3.7 的执行方式。然后打开
<http://0.0.0.0:80/ajax/libs/jquery/1.7.1/jquery.min.js> 测试能够打得开，而且内容和
<http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js> 一致。

下面修改 host，参考 [解决GitHub访问慢方案](../../analysis/github/quick-visit)。
