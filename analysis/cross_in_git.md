cross in Git
================

刚建立的 Rproj，没有 Git，所以会把上一个路径的git 调用出来，这里使用 `git status` 就可以看出来。 只要 `git
init` 后就不会了。

    $ git status
    On branch master
    Changes not staged for commit:
      (use "git add/rm <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)
      (commit or discard the untracked or modified content in submodules)
    
            modified:   ../../imp_rmd/.gitignore
            modified:   ../../imp_rmd/4cb.Rmd
            modified:   ../../imp_rmd/a20181123.Rmd
            deleted:    ../../imp_rmd/a20181127.Rmd
            deleted:    ../../imp_rmd/a20181127_2.Rmd

    $ git init
    Initialized empty Git repository in xxx/.git/
    $ git status
    On branch master
    
    No commits yet
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)
    
            .Rproj.user/
            config.toml
            content/
            index.Rmd
            sample_proj.Rproj
            static/
            themes/
    
    nothing added to commit but untracked files present (use "git add" to track)
