Git\* 学习笔记
================

<!-- README.md is generated from README.Rmd. Please edit that file -->

### To-do

1.  <input type="checkbox" id="checkbox1" class="styled">整合之前的笔记
2.  <input type="checkbox" id="checkbox1" class="styled">整合 imp\_rmd
    中的笔记
3.  更新没有 title 的 Rmd

### 目录

1.  这个repo主要是保存和分享提问的问题，为了后期迭代和查询。
2.  更新记录见[NEWS](NEWS.md)
3.  目前文章为13篇，具体见目录

<!-- end list -->

``` r
devtools::load_all("../add2md")
get_toc2('.')
```

<table>

<thead>

<tr>

<th style="text-align:right;">

index

</th>

<th style="text-align:left;">

hyperlink

</th>

<th style="text-align:left;">

github.io

</th>

</tr>

</thead>

<tbody>

<tr>

<td style="text-align:right;">

1

</td>

<td style="text-align:left;">

<a href="bfg.md" style="     " >使用 BFG 完成 Git 历史记录清理</a>

</td>

<td style="text-align:left;">

<a href="bfg" style="     " >使用 BFG 完成 Git 历史记录清理</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

2

</td>

<td style="text-align:left;">

<a href="bfg_bug.Rmd" style="     " >bfg\_bug</a>

</td>

<td style="text-align:left;">

<a href="bfg_bug" style="     " >bfg\_bug</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

3

</td>

<td style="text-align:left;">

<a href="bfg_cd.md" style="     " >当前路径删除大文件</a>

</td>

<td style="text-align:left;">

<a href="bfg_cd" style="     " >当前路径删除大文件</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

4

</td>

<td style="text-align:left;">

<a href="cross_in_git.md" style="     " >cross in Git</a>

</td>

<td style="text-align:left;">

<a href="cross_in_git" style="     " >cross in Git</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

5

</td>

<td style="text-align:left;">

<a href="delete_git.md" style="     " >delete\_git</a>

</td>

<td style="text-align:left;">

<a href="delete_git" style="     " >delete\_git</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

6

</td>

<td style="text-align:left;">

<a href="feature_github.md" style="     " >The fearure of Github</a>

</td>

<td style="text-align:left;">

<a href="feature_github" style="     " >The fearure of Github</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

7

</td>

<td style="text-align:left;">

<a href="git.md" style="     " >Git 学习笔记</a>

</td>

<td style="text-align:left;">

<a href="git" style="     " >Git 学习笔记</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

8

</td>

<td style="text-align:left;">

<a href="github_pages.md" style="     " >github pages 相关问题</a>

</td>

<td style="text-align:left;">

<a href="github_pages" style="     " >github pages 相关问题</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

9

</td>

<td style="text-align:left;">

<a href="gitlab.md" style="     " >gitLab</a>

</td>

<td style="text-align:left;">

<a href="gitlab" style="     " >gitLab</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

10

</td>

<td style="text-align:left;">

<a href="index.Rmd" style="     " >index</a>

</td>

<td style="text-align:left;">

<a href="index" style="     " >index</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

11

</td>

<td style="text-align:left;">

<a href="index_file_corrupt.Rmd" style="     " >index file corrupt</a>

</td>

<td style="text-align:left;">

<a href="index_file_corrupt" style="     " >index file corrupt</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

12

</td>

<td style="text-align:left;">

<a href="install-package-from-github.md" style="     " >Install Package
From Github</a>

</td>

<td style="text-align:left;">

<a href="install-package-from-github" style="     " >Install Package
From Github</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

13

</td>

<td style="text-align:left;">

<a href="install-r-branch.Rmd" style="     " >安装 R 包对应 branch</a>

</td>

<td style="text-align:left;">

<a href="install-r-branch" style="     " >安装 R 包对应 branch</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

14

</td>

<td style="text-align:left;">

<a href="nano.md" style="     " >nano</a>

</td>

<td style="text-align:left;">

<a href="nano" style="     " >nano</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

15

</td>

<td style="text-align:left;">

<a href="private_repo_gh_pages.md" style="     " >Github 私人项目 github
pages 注意事项</a>

</td>

<td style="text-align:left;">

<a href="private_repo_gh_pages" style="     " >Github 私人项目 github pages
注意事项</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

16

</td>

<td style="text-align:left;">

<a href="pull.Rmd" style="     " >pull</a>

</td>

<td style="text-align:left;">

<a href="pull" style="     " >pull</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

17

</td>

<td style="text-align:left;">

<a href="push_fail.Rmd" style="     " >git push gitlab master 失败</a>

</td>

<td style="text-align:left;">

<a href="push_fail" style="     " >git push gitlab master 失败</a>

</td>

</tr>

<tr>

<td style="text-align:right;">

18

</td>

<td style="text-align:left;">

<a href="sub-directory-push.Rmd" style="     " >子路径也可以 push</a>

</td>

<td style="text-align:left;">

<a href="sub-directory-push" style="     " >子路径也可以 push</a>

</td>

</tr>

</tbody>

</table>
