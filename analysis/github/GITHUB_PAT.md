GITHUB\_PAT 设置
================
Jiaxiang Li
2019-03-04

最近尝试使用 git2r 包批量 pull 若干项目，但是产生了报错，我提交了 [Github
Issue 382](https://github.com/ropensci/git2r/issues/382)，主要报错为

    > git2r::pull(".")
    Error in fetch(repo = repo, name = branch_remote_name(upstream_branch),  : 
      Error in 'git2r_remote_fetch': Unable to authenticate with supplied credentials

> If you Git push or pull via the command line, you can avoid the
> constant authentication challenge by caching your username and
> password for HTTPS access or by setting up SSH keys. This includes any
> Git operations done by RStudio on your behalf.
> [happygitwithr](https://happygitwithr.com/github-pat.html)

最终在发现是需要设置 `GITHUB_PAT`，参考
[happygitwithr](https://happygitwithr.com/github-pat.html)

使用

``` r
usethis::edit_r_environ()
```

打开进行设置。

> Put a line break at the end\! If you’re using an editor that shows
> line numbers, there should be two lines, where the second one is
> empty.

注意多空一行。

``` r
Sys.getenv("GITHUB_PAT")
```

可查询

> It’s a string of 40 random letters and digits

这是一个40位的随机码

最后可以执行

``` r
git2r::pull(credentials = git2r::cred_token())
```
