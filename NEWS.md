### 2019-04-20 01:23:05

- [x] 增加 cite 5b0a3764251cbac3d4bb4571ec5fe25c9217a923

### 2019-04-15 11:52:11

1. update images in the learn-ask-question-github

### 2019-04-14 14:19:57

1. add learn-ask-question-github

### 2019-04-10 21:05:11

add 

1. analysis/anaconda-prompt-copy.Rmd
1. analysis/cd-d-disk.Rmd

### 2019-04-10 13:06:29

1. add write-rmd-in-github.Rmd

### 2019-04-08 17:36:40

1. update add-license

### 2019-04-08 11:17:26

1. add add-license
1. merge news

### 2019-04-06 09:51:40

1. add git pull origin

### 2019-04-05 11:03:12

1. add 在大项目中完成 cbr

### 2019-04-01 23:39:24

1. add cherry-pick.Rmd
1. merge

### 2019-04-01 21:21:23

1. add tidy-git-ana.Rmd

### 2019-04-01 11:07:25

add No such file or directory

### 2019-03-21 16:15:17

1. 分配好文件到对应的文件夹
1. build fail

### 2019-03-21 15:04:13

1. add 版本迭代的进行数据科学项目

### 2019-03-18 10:58:54

1. add conda

### 2019-03-17 13:07:56

1. add linux 常用命令总结
1. merge

### 2019-03-11 16:55:57

1. add preview rmd

### 2019-03-04 18:03:05

1. add GITHUB_PAT 设置

### 2019-03-04 14:47:13

1. update toc

### 2019-02-23 23:01:15

1. add Install Package From Github

### 2019-02-16 12:40:46

1. add Search code in Github

### 2019-02-06 23:48:40

1. update toc

### 2019-02-06 23:18:15

1. commit

### 2019-02-01 15:05:40

1. 4 push

### 2019-01-25 13:11:11

1. mkdir gitlab
1. add gitlab pages pending 问题

### 2019-01-21 11:07:45

1. 更新目录

### 2019-01-21 00:36:02

1. add index.Rmd
1. add Github 私人项目 github pages 注意事项
1. 更新目录，但是不够优秀，因为不是文档的名字
1. add _output.yaml

### 2019-01-20 23:39:04

1. update readme md
1. merge

### 2019-01-12 15:46:02

1. add news
1. add html test
1. delete packrat
1. add github 展示 html 文件

### 2019-01-05 14:21:18

1. 增加 [cross in Git](cross_in_git.md)

### 2018-12-31 12:14:41

1. remove "vijadeMacBook-Pro:test_bfg.git vija"

### 2018-12-31 11:38:35

1. add 使用 BFG 完成 Git 历史记录清理

### 2018-12-30 15:37:06

1. add 
```{r}
"delete_git.md"
```


### 2018-12-28 09:42:15

1. merge files
1. add Nano How to use?

### 2018-12-13 22:35:11

1. add The fearure of Github

### 2018-12-05 00:40:44

1. 更新 push 卡住

### 2018-12-03 00:46:27

1. 新建 branch 前注意事项

### 2018-12-02 21:12:52

1. nano exit

### 2018-12-02 13:11:47

1. push 卡住

### 2018-11-30 19:01:43

1. 增加 ignore 的使用方式

### 2018-11-29 14:34:42

1. 增加 Branch的内容
1. DataCamp 课程完成

### 2018-11-26 20:32:59

1. 增加闪退处理方式

### 2018-11-25 20:37:45

1. 增加 DataCamp 中 Git 的笔记，除去branch的内容，因为不太重要。
