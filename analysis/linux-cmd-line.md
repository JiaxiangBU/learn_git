linux 常用命令总结
================

1.  `cp -R folder-1 folder-2`([Stack
    Overflow](https://stackoverflow.com/questions/14922562/how-do-i-copy-folder-with-files-to-another-folder-in-unix-linux))
    加入参数`-R`复制文件夹
2.  `mkdir results-$(date
    +%F)`([github](https://github.com/moldach/project-directory))产生`results-2019-03-17`

更多参考 [Blog](https://jiaxiangli.netlify.com/2018/01/04/shell/)
