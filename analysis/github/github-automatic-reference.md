Github Automatic Reference
================

github-automatic-reference.Rmd

我有一个私人项目，在commit 的时候，加入了某个github的issue， 发现居然在github上的最近活动中体现了

``` r
library(knitr)
include_graphics("figure/github-recent-activity.png")
```

![](figure/github-recent-activity.png)<!-- -->

并且可以看到相关的Issue中也 related 了。

``` r
include_graphics("figure/github-reference.png")
```

![](figure/github-reference.png)<!-- -->
