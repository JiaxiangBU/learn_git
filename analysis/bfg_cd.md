当前路径删除大文件
================

如出现如下报错

    $ git push
    Enumerating objects: 389, done.
    Counting objects: 100% (385/385), done.
    Delta compression using up to 4 threads
    Compressing objects: 100% (372/372), done.
    Writing objects: 100% (380/380), 142.65 MiB | 2.62 MiB/s, done.
    Total 380 (delta 138), reused 1 (delta 0)
    remote: Resolving deltas: 100% (138/138), completed with 2 local objects.
    remote: error: GH001: Large files detected. You may want to try Git Large File Storage - https://git-lfs.github.com.
    remote: error: Trace: c2d7da981f786ca2477897234efa8fd0
    remote: error: See http://git.io/iEPt8g for more information.
    remote: error: File proj_archive/fcontest/xiaosong_sent_files/tojxg/round2.2_full_data_ipt_miss_wo_scale_part2.csv is 222.08 MB; this exceeds GitHub's file
     size limit of 100.00 MB
    remote: error: File proj_archive/fcontest/xiaosong_sent_files/tojxg/round2.2_full_data_ipt_miss_wo_scale_part1.csv is 222.83 MB; this exceeds GitHub's file
     size limit of 100.00 MB
    To https://github.com/JiaxiangBU/imp_rmd.git
     ! [remote rejected] master -> master (pre-receive hook declined)
    error: failed to push some refs to 'https://github.com/xxx/xxx.git'

    cd 当前路径

输入类似的命令

    java -jar ~/Downloads/bfg-1.13.0.jar --delete-files round2.2_full_data_ipt_miss_wo_scale_part1.csv .git
    java -jar ~/Downloads/bfg-1.13.0.jar --delete-files round2.2_full_data_ipt_miss_wo_scale_part2.csv .git

注意这里不要加上之前的路径

    Error: *** Can only match on filename, NOT path *** - remove '/' path segments

直接写文件名就好，这里我好奇，万一文件名字同名了怎么办。
