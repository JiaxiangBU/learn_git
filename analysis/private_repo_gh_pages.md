Github 私人项目 github pages 注意事项
================
李家翔
2019-01-21

参考
[help](https://help.github.com/articles/page-build-failed-file-is-not-properly-utf-8-encoded/)

网站会发生 encoding
    问题，报错如下

    Your site is having problems building: The file pc_old_repo/CNY/detect is not properly UTF-8 encoded. For more information, see https://help.github.com/articles/page-build-failed-file-is-not-properly-utf-8-encoded/.

对于 private 项目， 当 encoding 问题全部解决后，都可以展示了。

1.  README 就可以展示了。
2.  master下的html 也可以展示了。
3.  所有的文件都可以在对应的链接下下载。
    1.  如 xls 等
4.  但是其他 `md` 是代码，不是富文本。
5.  文件夹是打不开的
