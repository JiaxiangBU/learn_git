使用 BFG 完成 Git 历史记录清理
================

注意看，即使删除文件，历史记录还是有敏感信息。

``` r
knitr::include_graphics("pic/history_contain_info.png")
```

![](pic/history_contain_info.png)<!-- -->

我们把Github上托管的项目，下载镜像。 等于不下载大文件，而只是 `.git`。

``` git
$ git clone --mirror https://github.com/JiaxiangBU/test_bfg.git
Cloning into bare repository 'test_bfg.git'...
remote: Enumerating objects: 28, done.
remote: Counting objects: 100% (28/28), done.
remote: Compressing objects: 100% (14/14), done.
remote: Total 28 (delta 13), reused 26 (delta 11), pack-reused 0
Unpacking objects: 100% (28/28), done.
```

备份，

<input type="checkbox" id="checkbox1" class="styled">备份路径 shell

``` shell
cp ... ...
```

下载 bfg，见
[github](https://help.github.com/articles/removing-sensitive-data-from-a-repository/),
[rtyley](https://rtyley.github.io/bfg-repo-cleaner/) 语言通过 scala 完成。

``` shell
cd 对应路径
```

`java -jar ~/Downloads/bfg-1.13.0.jar --delete-files
i_want_to_delete_it.R` ~~`test_bfg.git`~~

`~/Downloads/bfg-1.13.0.jar`这里给清楚 bfg 的地址。

``` git

$ java -jar ../bfg-1.13.0.jar --delete-files i_want_to_delete_it.R

Using repo : /Users/vija/Downloads/180805_folder_01/tmp_jli/trans/projIN/tmp_mirror/test_bfg.git

Found 6 objects to protect
Found 2 commit-pointing refs : HEAD, refs/heads/master

Protected commits
-----------------

These are your protected commits, and so their contents will NOT be altered:

 * commit f04d6c3d (protected by 'HEAD')

Cleaning
--------

Found 4 commits
Cleaning commits:       100% (4/4)
Cleaning commits completed in 89 ms.

Updating 1 Ref
--------------

    Ref                 Before     After   
    ---------------------------------------
    refs/heads/master | f04d6c3d | 71139798

Updating references:    100% (1/1)
...Ref update completed in 18 ms.

Commit Tree-Dirt History
------------------------

    Earliest      Latest
    |                  |
      .    .    D    m  

    D = dirty commits (file tree fixed)
    m = modified commits (commit message or parents changed)
    . = clean commits (no changes to file tree)

                            Before     After   
    -------------------------------------------
    First modified commit | 2aed1451 | b9c4b8dd
    Last dirty commit     | 2aed1451 | b9c4b8dd

Deleted files
-------------

    Filename                Git id         
    ---------------------------------------
    i_want_to_delete_it.R | 13792e77 (19 B)


In total, 3 object ids were changed. Full details are logged here:

    /Users/vija/Downloads/180805_folder_01/tmp_jli/trans/projIN/tmp_mirror/test_bfg.git.bfg-report/2018-12-31/10-41-59

BFG run is complete! When ready, run: git reflog expire --expire=now --all && git gc --prune=now --aggressive


--
You can rewrite history in Git - don't let Trump do it for real!
Trump's administration has lied consistently, to make people give up on ever
being told the truth. Don't give up: https://www.aclu.org/
--
```

参考 [rtyley](https://rtyley.github.io/bfg-repo-cleaner/)

``` 

$ git reflog expire --expire=now --all && git gc --prune=now --aggressive
Counting objects: 15, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (13/13), done.
Writing objects: 100% (15/15), done.
Total 15 (delta 4), reused 0 (delta 0)
```

``` git
git push
```

<input type="checkbox" id="checkbox1" class="styled">可以研究下 bfg 的 report

再查看原来的commit， `i_want_to_delete_it.R`的记录已经删除。

``` r
knitr::include_graphics("pic/history_contain_info_clean.png")
```

![](pic/history_contain_info_clean.png)<!-- -->
