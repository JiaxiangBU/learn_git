
当新建一个项目， 发现`git status`出现了很多母路径 `../`的文件， 参考 [cross in
Git](cross_in_git.md)。 这个时候如果去commit，会把这些文件也commit上去不好。
最好的方式是删除`.git`文件，再重建。

``` r
knitr::include_graphics("pic/delete_git.png")
```

![](pic/delete_git.png)<!-- -->

    $ git init
    Initialized empty Git repository in .../.git/
    $ git status
    On branch master
    
    No commits yet
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)
    
            .Rbuildignore
            .gitignore
            CODE_OF_CONDUCT.md
            DESCRIPTION
            LICENSE.md
            NAMESPACE
            NEWS.md
            README.Rmd
            README.md
            regex4impala.Rproj
    
    nothing added to commit but untracked files present (use "git add" to track)

``` r
knitr::include_graphics("pic/show_history_github.png")
```

![](pic/show_history_github.png)<!-- -->
