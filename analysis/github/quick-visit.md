解决GitHub访问慢方案
================

参考 [blog](https://blog.csdn.net/wu__di/article/details/50538916)

打开 IPAddress.com，输入 github.com 和 github.global.ssl.fastly.net，查找IP地址 如
`192.168.xx.xx` 和 `185.31.17.xx`

打开 Terminal，输入

``` shell
sudo vi /etc/hosts
```

输入密码。 按<kbd>i</kbd>进入编辑模式， 添加 host

``` shell
192.168.xx.xx github.com
185.31.17.xx github.global.ssl.fastly.net
```

按住 <kbd>esc</kbd> 退出， 按住 <kbd>shift</kbd> + <kbd>:</kbd> 进入命令模式，输入
<kbd>w</kbd> + <kbd>q</kbd> 退出。

输入

``` shell
sudo dscacheutil -flushcache
```

刷新缓存，搞定。
