学习使用 MileStone
================

> A **milestone** acts like a container for issues
> 
> Once you’ve **collected a lot of issues**, you may find it hard to
> find the ones you care about. **Milestones, labels, and assignees**
> are great features to filter and categorize issues.
> 
> **Milestones** are groups of issues that correspond to a **project,
> feature, or time period**.
> [guides.github.com](https://guides.github.com/features/issues/)

milestone 类似于 issues 的容器，主要功能也是作为一个筛选的标签，一般设置可以根据软件的特性、项目和时间周期来设定。

如

> **October Sprint** — File issues that you’d like to work on in
> October. A great way to focus your efforts when there’s a lot to do.

表示十月份的冲刺，任务安排在10月份完成。

并且 MileStone 中的任务可以使用鼠标进行拖动。
[help.github.com](https://help.github.com/en/articles/about-milestones#prioritizing-issues-and-pull-requests-in-milestones)

![](https://camo.githubusercontent.com/f26db80d382a984391d1666d44565cdae359e449/68747470733a2f2f68656c702e6769746875622e636f6d2f6173736574732f696d616765732f68656c702f6973737565732f6d696c6573746f6e652d72656f7264657265642e676966)
