Git 学习笔记
================

> git diff, git status, git add and git commit

这是 git 最基本的四个命令，建议熟练掌握，之后按需学习。

# git diff

``` r
knitr::include_graphics(file.path('pic','staging_area.png'))
```

![](pic/staging_area.png)<!-- -->

1.  在`git status`阶段是 staging files
2.  在`add`后，是staged files，这一步也叫做 [Add one or more files to the staging
    area.](https://campus.datacamp.com/courses/introduction-to-git-for-data-science/basic-workflow?ex=6)
    1.  `git add filename`
3.  `git diff`反映的是 staging files
    1.  `git diff` 查看所有变化
    2.  `git diff filename`
    3.  `git diff directory`

<!-- end list -->

    $ git diff
    diff --git a/data/northern.csv b/data/northern.csv
    index 5eb7a96..5a2a259 100644
    --- a/data/northern.csv
    +++ b/data/northern.csv
    @@ -22,3 +22,4 @@ Date,Tooth
     2017-08-13,incisor
     2017-08-13,wisdom
     2017-09-07,molar
    +2017-11-01,bicuspid

结果解释

1.  第一行 `diff --git a/data/northern.csv b/data/northern.csv` 表示哪个文件修改
2.  `@@ -22,3 +22,4 @@ Date,Tooth`表示
    1.  **去掉从第22行起，一共3行改变；增加从第22行起，一共四行改变**

## git diff -r HEAD

1.  `-r` flag means “compare to a particular revision”
2.  `HEAD` is a shortcut meaning “the most recent commit”.

`git diff -r HEAD
path/to/file`

<https://campus.datacamp.com/courses/introduction-to-git-for-data-science/basic-workflow?ex=8>

## git diff ID1..ID2

对比两个commit的差异

1.  `git diff abc123..def456`
2.  `git diff HEAD~1..HEAD~3`

# nano

`nano filename`

1.  打开已存在的文件
2.  创建未存在的文件

1 方向键控制方向，删除键删除1. 方向键控制方向，删除键删除 1. Ctrl-K: delete a line. 1. Ctrl-U:
un-delete a line. 1. Ctrl-O: save the file (‘O’ stands for ‘output’). 1.
Ctrl-X: exit the editor. 1. nano exit <kbd>command</kbd>+<kbd>z</kbd>

已经成功测试`nano`。

## Git launches a text editor

1.  只输入`git commit`，不加`-m`
2.  写 readable 的message
3.  通过 Ctrl-O 和 Ctrl-X 来完成

# git commit

    git commit -m "Program appears to have become self-aware."

    git commit --amend -m "new message"

1.  当上一条commit写错了，这条可以修改
2.  注意`- m`中间有空格

<!-- end list -->

``` git
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   report.txt
```

git 需要commit了。

# git log

``` git
$ git log
commit 2e3b95ed8bdbcbe08c2dfbce318027085d09f597 (HEAD -> master, origin/master, origin/HEAD)
Merge: ef8cb3e 38ac7e2
Author: Jiaxiang Li <alex.lijiaxiang@foxmail.com>
Date:   Fri Nov 23 23:21:41 2018 +0800

    merge

commit ef8cb3e8d8b00eb460b9dceb84bceef049b41caa
Author: Jiaxiang Li <alex.lijiaxiang@foxmail.com>
Date:   Fri Nov 23 19:48:16 2018 +0800

    commit for merge before
```

1.  `q`退出
2.  `ef8cb3e8d8b00eb460b9dceb84bceef049b41caa` 是 hash

<details>

<summary>hash is a unique identifier</summary>

1.  a pseudo-random number generator called a hash function
2.  written as a 40-character hexadecimal string
3.  但是实际上只需要看前1. 但是实际上只需要看前6-8位

</details>

`git log path`

`git log -3 filename` 最近三条

# git show

查看commit

``` git
$ git show
commit 9a8320c4b8e2765aae74ce55f44038248651e95aAuthor: Rep Loop <repl@datacamp.com>
Date:   Thu Sep 13 13:18:14 2018 +0000

    Added year to report title.

diff --git a/report.txt b/report.txt
index e713b17..4c0742a 100644
--- a/report.txt
+++ b/report.txt
@@ -1,4 +1,4 @@
-# Seasonal Dental Surgeries 2017-18
+# Seasonal Dental Surgeries (2017) 2017-18

 TODO: write executive summary.
```

其中包含了git diff 的内容，见 `diff --git a/report.txt b/report.txt`。

## HEAD\~1

``` git
$ git show HEAD~1
commit 65f236a2ec2e6cd80e77a6c37c2d6b24b2707907
Author: Rep Loop <repl@datacamp.com>
Date:   Thu Sep 13 13:18:14 2018 +0000

    Adding fresh data for western region.

diff --git a/data/western.csv b/data/western.csv
index f6d6374..f7c4509 100644
--- a/data/western.csv
+++ b/data/western.csv
@@ -27,3 +27,6 @@ Date,Tooth
 2017-10-05,molar
 2017-10-06,incisor
 2017-10-07,incisor
+2017-10-15,molar
+2017-10-17,bicuspid
+2017-10-18,bicuspid
```

  - `HEAD~1`  
    just before the most recent one

# git annotate

``` git
$ git annotate report.txt
9a8320c4        (  Rep Loop     2018-09-13 13:18:14 +0000       1)# Seasonal Dental Surgeries (2017) 2017-18
56f80e3e        (  Rep Loop     2018-09-13 13:18:14 +0000       2)
56f80e3e        (  Rep Loop     2018-09-13 13:18:14 +0000       3)TODO: write executive summary.
56f80e3e        (  Rep Loop     2018-09-13 13:18:14 +0000       4)
56f80e3e        (  Rep Loop     2018-09-13 13:18:14 +0000       5)TODO: include link to raw data.
7f4b3efa        (  Rep Loop     2018-09-13 13:18:14 +0000       6)
7f4b3efa        (  Rep Loop     2018-09-13 13:18:14 +0000       7)TODO: remember to cite funding sources!
```

可以查看作者

1.  The first eight digits of the hash, `04307054.`
2.  The author, `Rep Loop`.
3.  The time of the commit, `2017-09-20 13:42:26 +0000`.
4.  The line number, `1`.
5.  The contents of the line, `# Seasonal Dental Surgeries
    (2017) 2017-18`.

# git add

1.  没有 add 的文件，是不会被 track 的，因此无法进行版本控制。所以可以使用`git status`进行查看。 \[Wilson
    (2017)

<!-- end list -->

  - [How do I add new files? |
    Shell](https://campus.datacamp.com/courses/introduction-to-git-for-data-science/repositories?ex=7)
    \]

<!-- end list -->

1.  git add 可以让对同一文件的修改，进行分别的commit，从而进行版本控制。

# .gitignore

如果含有

``` git
build
*.mpl
```

那么 Git 不会同步 `build`文件夹，和`*.mpl`类型的文件。

# git clean -n

对于 untracked 文件，可以进行删除 `git clean -f`执行

# git config

``` git
git config --global setting.name setting.value
git config --global user.name Jiaxiang Li
git config --global user.email alex.lijiaxiang@foxmail.com
```

# git reset

``` git
$ git status
On branch masterChanges not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   data/eastern.csv
        modified:   data/northern.csv

no changes added to commit (use "git add" and/or "git commit -a")
$ git add -A
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   data/eastern.csv
        modified:   data/northern.csv
```

当某个文件被错误的add了，可以通过`git reset HEAD`还原到 unstaged 的状态。

``` git
$ git reset HEAD
Unstaged changes after reset:
M       data/eastern.csv
M       data/northern.csv
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   data/eastern.csv
        modified:   data/northern.csv

no changes added to commit (use "git add" and/or "git commit -a")
```

`git reset HEAD filename`中未指定对应的文件和路径，就是全部staged文件执行unstage命令 `git
checkout -- .`对当前路径所有unstaged文件进行撤销修改。 常用的场景是，本地还没有保存，但是不小心git
pull，导致本地文件被覆盖了，这时只需要`git checkout -- .`，还原后，再`git pull`就好了。
但是每次在git pull 前，要先commit 所有本地的change，因此每次git pull 前先commit 是好习惯。

# git checkout

对unstaged 的文件使用 `git checkout -- filename`撤销编辑 例如 `git checkout --
data/northern.csv`

因此两者搭配可以处理对staged 的文件撤销编辑。

``` git
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        modified:   data/eastern.csv
        modified:   data/northern.csv

$ git reset data/northern.csv
Unstaged changes after reset:
M       data/northern.csv
$ git checkout -- data/northern.csv
.git/       bin/        data/       report.txt  results/
$ git checkout -- data/northern.csv
```

## 版本恢复

使用`git log`

``` git
commit ab8883e8a6bfa873d44616a0f356125dbaccd9ea
Author: Author: Rep Loop <repl@datacamp.com>
Date:   Thu Oct 19 09:37:48 2017 -0400

    Adding graph to show latest quarterly results.

commit 2242bd761bbeafb9fc82e33aa5dad966adfe5409
Author: Author: Rep Loop <repl@datacamp.com>
Date:   Thu Oct 16 09:17:37 2017 -0400

    Modifying the bibliography format.
```

1.  `git checkout 2242bd report.txt`恢复到对应版本
2.  再进行 commit

# git init

``` git
$ pwd
/home/repl/dental
$ git init
Initialized empty Git repository in /home/repl/dental/.git/
```

# git clone

会将源文件的历史记录一起复制。

同时也可以复制本地文件

``` git
git clone /existing/project newprojectname
```

# git remote

查看 clone 的源头

``` git
$ git remote -v
origin  /home/thunk/repo (fetch)
origin  /home/thunk/repo (push)
```

``` git
$ git remote -v
origin  https://github.com/JiaxiangBU/imp_rmd.git (fetch)
origin  https://github.com/JiaxiangBU/imp_rmd.git (push)
```

# git pull

``` git
$ git push origin dental
error: src refspec dental does not match any.
error: failed to push some refs to '/home/thunk/repo'
$ git push origin master
To /home/thunk/repo
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to '/home/thunk/repo'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
$ git pull
Merge made by the 'recursive' strategy.
```

经常git push 时，会产生报错，因此需要git pull，然后

> bring your repository up to date with `origin.` It will open up an
> editor that you can exit with Ctrl+X.

如不想弹出编辑器，输入

``` git
$ git pull --no-edit origin master
```

## other-branch

``` git
$ git pull origin other-branch
```

参考 [Stack
Overflow](https://stackoverflow.com/questions/1709177/git-pull-a-certain-branch-from-github)

# git branch

branch 最直观的理解是

Two commits have more than one parent.

## 新建 branch 前注意事项

1.  先commit + push，否则merge不成功时，可以再merge。
2.  而且从断舍离的角度，会把 merged branch 删除，如果之前已经push了，那么云端还有报错的，可以push下来。

## 查看 branch 的差异

``` git
$ cd dental$ git diff summary-statistics master
diff --git a/bin/summary b/bin/summarydeleted file mode 100755
...
```

每个diff会用`diff --git`开头展示。

## 配合 git rm 使用

``` git
$ git checkout -b deleting-report
Switched to a new branch 'deleting-report'
$ git rm report.txt
rm 'report.txt'
$ git commit -m 'delete report.txt'
[deleting-report 97cb6e1] delete report.txt
 1 file changed, 7 deletions(-)
 delete mode 100644 report.txt
$ git diff master..deleting-report
diff --git a/report.txt b/report.txt
deleted file mode 100644
index 4c0742a..0000000
--- a/report.txt
+++ /dev/null
@@ -1,7 +0,0 @@
-# Seasonal Dental Surgeries (2017) 2017-18
-
-TODO: write executive summary.
-
-TODO: include link to raw data.
-
-TODO: remember to cite funding sources!
```

这样不会对master 误删文件产生不良效果。

## git merge source destination

`destination` 可省略。

``` git
<<<<<<< destination-branch-name
...changes from the destination branch...
=======
...changes from the source branch...
>>>>>>> source-branch-name
```

1.  大多时候`destination-branch-name`为`HEAD`，表示 Current branch
2.  使用`git status`查看需要处理 conflict的文件

# git rm

这里举一个例子， 在不同的branch中，删除某个branch的一个文件。

``` git
$ cd dental$ git branch
  alter-report-title* master
  summary-statistics
$ git checkout summary-statistics
Switched to branch 'summary-statistics'
$ ls
bin  data  report.txt  results
$ git rm report.txt
rm 'report.txt'
$ ls
bin  data  results
$ git commit -m 'rm report'
[summary-statistics c845641] rm report
 1 file changed, 7 deletions(-)
 delete mode 100644 report.txt
$ git checkout master
Switched to branch 'master'
$ ls
bin  data  report.txt  results
```

# .giitignore

``` git
touch .giitignore
```

## 和 git status 合用

当敏感数据在文件夹时， 需要使用`.giitignore`文件进行ignore 这时可以不断申明后，查看git
status，直到没有敏感数据为止。

# 相关问题

## Submodule

> One thing you should not do is create one Git repository inside
> another. While Git does allow this, updating nested repositories
> becomes very complicated very quickly, since you need to tell Git
> which of the two .git directories the update is to be stored in. Very
> large projects occasionally need to do this, but most programmers and
> data analysts try to avoid getting into this situation. (Wilson 2017)

最好不要嵌套，这样逻辑会非常复杂，除非是大项目。

但是根据 Chacon and Straub (2014) 的介绍也不会太过于复杂。

## create a new repository on the command line

``` git
echo "# rong360" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/JiaxiangBU/rong360.git
git push -u origin master
```

## push an existing repository from the command line

``` git
git remote add origin https://github.com/JiaxiangBU/rong360.git
git push -u origin master
```

## push文件过大异常

参考 [CSDN博客](https://blog.csdn.net/qq_21770005/article/details/78733008)
、[Atlassian
Documentation](https://confluence.atlassian.com/stashkb/git-push-fails-fatal-the-remote-end-hung-up-unexpectedly-282988530.html)

``` git
error: RPC failed; curl 56 LibreSSL SSL_read: SSL_ERROR_SYSCALL, errno 60
fatal: The remote end hung up unexpectedly
```

需要用VPN等(如蓝灯)。 2018-12-11 11:40:44 成功一次

参考
[CSDN博客](https://blog.csdn.net/zang141588761/article/details/81352123)

``` git
git config http.postBuffer 524288000
```

设置通信缓存。

参考 [Github
Help](https://help.github.com/articles/removing-files-from-a-repository-s-history/)

``` git
$ git rm --cached giant_file
# Stage our giant file for removal, but leave it on disk
$ git commit --amend -CHEAD
# Amend the previous commit with your change
# Simply making a new commit won't work, as you need
# to remove the file from the unpushed history as well
$ git push
```

<details>

<summary>这是一个例子。</summary>

``` git
$ git rm --cached  kaggle/PS_20174392719_1491204439457_log.csv
rm 'kaggle/PS_20174392719_1491204439457_log.csv'

$ git commit --amend -CHEAD
[master 680607e] knit rmd
 Date: Wed Nov 28 13:18:30 2018 +0800
 36 files changed, 233 insertions(+), 6362723 deletions(-)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-19-1.png (96%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-24-1.png (96%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-25-1.png (98%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-26-1.png (89%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-27-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-29-1.png (93%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-33-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-36-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-38-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-40-1.png (98%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-42-1.png (96%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-42-2.png (96%)
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-54-1.png
 rewrite datacamp_files/figure-gfm/unnamed-chunk-55-1.png (99%)
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-55-2.png
 rewrite datacamp_files/figure-gfm/unnamed-chunk-56-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-57-1.png (99%)
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-57-2.png
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-59-1.png
 rewrite datacamp_files/figure-gfm/unnamed-chunk-60-1.png (99%)
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-60-2.png
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-64-1.png
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-64-2.png
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-67-1.png
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-72-1.png
 rewrite datacamp_files/figure-gfm/unnamed-chunk-73-1.png (99%)
 rewrite datacamp_files/figure-gfm/unnamed-chunk-74-1.png (99%)
 create mode 100644 datacamp_files/figure-gfm/unnamed-chunk-76-1.png
 rewrite datacamp_files/figure-gfm/unnamed-chunk-77-1.png (99%)
 delete mode 100644 kaggle/PS_20174392719_1491204439457_log.csv
```

</details>

参考 [Github
Help](https://help.github.com/articles/removing-sensitive-data-from-a-repository/)

<details>

<summary>以大文件`kaggle/PS_20174392719_1491204439457_log.csv`为例。</summary>

``` git
git filter-branch --force --index-filter \
'git rm --cached --ignore-unmatch kaggle/PS_20174392719_1491204439457_log.csv' \
--prune-empty --tag-name-filter cat -- --all

git add .gitignore

git commit -m "Add kaggle/PS_20174392719_1491204439457_log.csv to .gitignore"

# Double-check that you've removed everything you wanted to from your repository's history, and that all of your branches are checked out.

git push origin --force --all
```

</details>

## 当电脑闪退时，git的报错

``` git
$ git status
error: bad signature
fatal: index file corrupt
```

参考
[这个博客](https://www.clarencep.com/2017/10/23/a-note-to-git-error-bad-signature-index-file-corrupt/)

## 删除Commit过的大文件

参考 [开源中国](https://my.oschina.net/u/248080/blog/474295)
这篇博客

``` git
git filter-branch -f --index-filter "git rm -rf --cached --ignore-unmatch .RDataTmp"
```

并且在`.gitignore`声明这个地址。

## Git push master fatal: You are not currently on a branch

参考 [Stack
Overflow](https://stackoverflow.com/questions/30471557/git-push-master-fatal-you-are-not-currently-on-a-branch)

``` git
git branch temp-branch
git checkout master
git merge temp-branch
git push origin master
```

## 同步过多/大文件

``` git
error: RPC failed; curl 18 transfer closed with outstanding read data remaining
fatal: The remote end hung up unexpectedly
fatal: early EOF
fatal: index-pack failed
```

``` git
git config --global http.postBuffer 524288000
```

``` git
git config --list
```

查看 [CSDN博客](https://blog.csdn.net/dzhongjie/article/details/81152983)

## fatal: refusing to merge unrelated histories

参考 [CSDN博客](https://blog.csdn.net/m0_37402140/article/details/72801372)

``` git
git pull origin master --allow-unrelated-histories
```

## push 卡住

<details>

<summary>卡住</summary>

``` git
$ git push
Counting objects: 43, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (41/41), done.
Writing objects: 100% (43/43), 22.58 MiB | 14.22 MiB/s, done.
Total 43 (delta 14), reused 0 (delta 0)
```

</details>

<details>

<summary>git push</summary>

``` git
$ git push
Counting objects: 43, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (41/41), done.
Writing objects: 100% (43/43), 22.58 MiB | 14.33 MiB/s, done.
Total 43 (delta 14), reused 0 (delta 0)
error: RPC failed; curl 56 LibreSSL SSL_read: SSL_ERROR_SYSCALL, errno 50
fatal: The remote end hung up unexpectedly
```

</details>

<details>

<summary>git pull</summary>

``` git
$ git pull
remote: Enumerating objects: 34, done.
remote: Counting objects: 100% (34/34), done.
remote: Compressing objects: 100% (23/23), done.
error: RPC failed; curl 56 LibreSSL SSL_read: SSL_ERROR_SYSCALL, errno 54
fatal: The remote end hung up unexpectedly
fatal: early EOF
fatal: unpack-objects failed
```

</details>

> This is because of long process running in the server side. [Stack
> Overflow](https://stackoverflow.com/questions/45468877/error-rpc-failed-curl-56-openssl-ssl-read-ssl-error-syscall-errno-10054)

几种办法

参考
[CSDN博客](https://blog.csdn.net/yemoweiliang/article/details/53082131)，
增加host，因为github的DNS被GFW墙

``` host
199.27.74.133 assets-cdn.github.com
```

Mac设置方式，

1.  <kbd>shift</kbd> + <kbd>command</kbd> + <kbd>G</kbd>
2.  路径为`etc/hosts`

修改设置通信缓存 参考 [GitHub Issue](https://github.com/lanlin/notes/issues/41)

``` git
git config http.postBuffer 524288000
git config https.postBuffer 524288000
```

切换成手机流量**中国电信**，重新`pull`和`push`。 只有电信的网络还不错。

## github\_document

github\_document 一个解决方案
`knitr::kable()`

# 参考

# 附录

## 完成证书

``` r
knitr::include_graphics('https://www.datacamp.com/statement-of-accomplishment/course/4d6b7d8dc4fb3dc080e19a7d4cc837bd6384a82b.pdf')
```

![](https://www.datacamp.com/statement-of-accomplishment/course/4d6b7d8dc4fb3dc080e19a7d4cc837bd6384a82b.pdf)<!-- -->

<div id="refs" class="references">

<div id="ref-Chacon2014">

Chacon, Scott, and Ben Straub. 2014. *Pro Git*. 2nd ed. 2014. Apress.
<http://gen.lib.rus.ec/book/index.php?md5=66a9e6367176a95b87bcbfaf3cef242a>.

</div>

<div id="ref-Wilson2017">

Wilson, Greg. 2017. “Introduction to Git for Data Science.” 2017.
<https://www.datacamp.com/courses/introduction-to-git-for-data-science>.

</div>

</div>
