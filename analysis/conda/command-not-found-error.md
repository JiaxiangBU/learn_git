Command Not Found Error
================
Jiaxiang Li
2019-03-18

    CommandNotFoundError: Your shell has not been properly configured to use 'conda activate'.
    If using 'conda activate' from a batch script, change your
    invocation to 'CALL conda.bat activate'.
    
    To initialize your shell, run
    
        $ conda init <SHELL_NAME>
    
    Currently supported shells are:
      - bash
      - cmd.exe
      - fish
      - tcsh
      - xonsh
      - zsh
      - powershell
    
    See 'conda init --help' for more information and options.
    
    IMPORTANT: You may need to close and restart your shell after running 'conda init'.

`conda init bash` 没有用 去 github 问问

参考 [Stack
Overflow](https://stackoverflow.com/questions/2518127/how-do-i-reload-bashrc-without-logging-out-and-back-in)
和 [Github Issue 8313](https://github.com/conda/conda/issues/8313) 使用
`source ~/.bashrc`即可
