目前发现 GitLab 不支持以下场景。

项目 A 被 forked 后产生 项目 A'。
在项目 A 更新后，产生若干 commits，不能提交 merge request 把更新的commits 合并到 A' 上。

因此开发者之间应该之后只维护项目 A，而非在跨项目之间进行 merge request。

目前的解决方案是在协作者的权限上升到 developer。
然后协作者 git clone 项目，在本地完成修改，git push 和 pull。

一般地，自建项目（不是 forked），都有一个 **protected** 的标签，这时候需要修改权限。

```{r, fig.cap="protected 标签"}
library(knitr)
include_graphics(here::here("refs/protected-tag.png"))
```

点开 project settings

```{r, fig.cap="设置 developer 的权限"}
include_graphics(here::here("refs/protect-setting.png"))
include_graphics(here::here("refs/protect-setting2.png"))
```



